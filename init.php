<?php
require './class';
function AutoLoad($class)
{
    if (file_exists("./class/" . $class . ".php")) {
        require "./class/" . $class . ".php";
    } elseif (file_exists("./class/repository" . $class . ".php")) {
        require "./class/repository" . $class . ".php";
    } else {
        exit("Le fichier $class.php n'existe ni dans classe ni dans repository.");
    }
}
spl_autoload_register("AutoLoad");
session_start();
