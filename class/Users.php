<?php
class Users
{

    private $_id,
        $_Firstname,
        $_Name,
        $_Mail,
        $_Password,
        $_Role,
        $_CreatedAt;

    public function __construct(array $params)
    {
        $this->hydrate($params);
    }

    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }
    // Getters & Setters

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->_id;
    }

    /**
     * @param  int $_id
     * @return self
     */
    public function setId(int $_id): self
    {
        $this->_id = $_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->_Firstname;
    }

    /**
     * @param  string $_Firstname
     * @return  self
     */
    public function setFirstname(string $_Firstname): self
    {
        $this->_Firstname = $_Firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->_Name;
    }

    /**
     * @param  string $_Name
     * @return  self
     */
    public function setName(string $_Name): self
    {
        $this->_Name = $_Name;

        return $this;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->_Mail;
    }

    /**
     * @param string $_Mail
     * @return  self
     */
    public function setMail(string $_Mail): self
    {
        $this->_Mail = $_Mail;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->_Password;
    }

    /**
     * @param string $_Password
     * @return  self
     */
    public function setPassword(string $_Password): self
    {
        $this->_Password = $_Password;

        return $this;
    }

    /**
     * @return bool
     */
    public function getRole(): bool
    {
        return $this->_Role;
    }

    /**
     * @param bool $_Role
     * @return  self
     */
    public function setRole(bool $_Role): self
    {
        $this->_Role = $_Role;

        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->_CreatedAt;
    }

    /**
     * @param DateTimeImmutable $_CreatedAt
     * @return  self
     */
    public function setCreatedAt($_CreatedAt): self
    {
        if ($_CreatedAt instanceof DateTimeImmutable) {
            $this->_CreatedAt = $_CreatedAt;
        } else {
            // Convertir la chaîne en objet DateTimeImmutable
            $this->_CreatedAt = new DateTimeImmutable($_CreatedAt);
        }

        return $this;
    }

    // Methods
    public function isConnected()
    {
    }
}
