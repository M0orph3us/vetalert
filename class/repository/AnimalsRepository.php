<?php
require '../../class/Animals.php';
require '../../class/Database.php';
class AnimalsRepository
{

    private $_db,
        $_injectedAt;

    public function __construct()
    {
        $this->_db = new Database();
        $this->_db = $this->_db->getBDD();

        $this->_injectedAt = new DateTimeImmutable();
    }

    // CRUD

    public function newAnimal(string $name, string $type, string $vaccine)
    {
        $sql = "INSERT INTO animals (name, type, vaccine, injectedAt) VALUES ( :name, :type, :vaccine, :injectedAt)";

        $params = [
            'name' => $name,
            'type' => $type,
            'vaccine' => $vaccine,
            'injectedAt' => $this->_injectedAt->format('Y-m-d H:i:s')
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
        $pdostmt->closeCursor();

        $animalId = $this->_db->lastInsertId();
        $params['id'] = $animalId;

        $animal = new Animals($params);
        return $animal;
    }

    public function findOneAnimal(int $id)
    {
        $sql = "SELECT * FROM animals WHERE id = :id";
        $params = [
            'id' => $id
        ];
        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
        return $pdostmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findAllAnimals()
    {
        $sql = "SELECT * FROM animals";
        $pdostmt = $this->_db->prepare($sql);
        return $pdostmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateAnimal(int $id, string $name, string $type, string $vaccine, DateTime $injectedAt)
    {
        $sql = "UPDATE animals SET name= :name, type= :type, vaccine= :vaccine, injectedAt= :injectedAt WHERE id= :id";
        $params = [
            'id' => $id,
            'name' => $name,
            'type' => $type,
            'vaccine' => $vaccine,
            'injectedAt' => $injectedAt
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
    }

    public function deleteAnimal(int $id)
    {
        $sql = "DELETE FROM animals WHERE id= :id";
        $params = [
            'id' => $id
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
    }
}
