<?php
require '../../class/Types.php';
require '../../class/Database.php';
class TypesRepository
{

    private $_db;

    public function __construct()
    {
        $this->_db = new Database();
        $this->_db = $this->_db->getBDD();
    }

    public function newType(string $specie)
    {
        $sql = "INSERT INTO types (specie) VALUES (:specie)";
        $params = ['specie' => $specie];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
        $pdostmt->closeCursor();

        $typeId = $this->_db->lastInsertId();
        $params['id'] = $typeId;

        $type = new Types($params);
        return $type;
    }

    public function findOneType(int $id)
    {
        $sql = "SELECT * FROM types WHERE id = :id";
        $params = [
            'id' => $id
        ];
        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
        return $pdostmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findAllTypes()
    {
        $sql = "SELECT * FROM types";
        $pdostmt = $this->_db->prepare($sql);
        return $pdostmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateType(int $id, string $specie)
    {
        $sql = "UPDATE types SET specie= :specie WHERE id= :id";
        $params = [
            'id' => $id,
            'specie' => $specie
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
    }

    public function deleteType(int $id)
    {
        $sql = "DELETE FROM types WHERE id= :id";
        $params = [
            'id' => $id
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
    }
}
