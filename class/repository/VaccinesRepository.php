<?php
require '../../class/Vaccines.php';
require '../../class/Database.php';
class VaccinesRepository
{

    private $_db;

    public function __construct()
    {
        $this->_db = new Database();
        $this->_db = $this->_db->getBDD();
    }

    public function newVaccine(string $name, DateTime $duration)
    {
        $sql = "INSERT INTO vaccines (name, duration) VALUES (:name, :duration)";

        $params = [
            'name' => $name,
            'duration' => $duration->format('Y-m-d H:i:s')
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
        $pdostmt->closeCursor();

        $vaccineId = $this->_db->lastInsertId();
        $params['id'] = $vaccineId;

        $vaccine = new Vaccines($params);
        return $vaccine;
    }

    public function findOneVaccine(int $id)
    {
        $sql = "SELECT * FROM vaccines WHERE id = :id";
        $params = [
            'id' => $id
        ];
        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
        return $pdostmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findAllVaccines()
    {
        $sql = "SELECT * FROM vaccines";
        $pdostmt = $this->_db->prepare($sql);
        return $pdostmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateVaccine(int $id, string $name, DateTime $duration)
    {
        $sql = "UPDATE vaccines SET name= :name, duration= :duration WHERE id= :id";
        $params = [
            'id' => $id,
            'name' => $name,
            'duration' => $duration
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
    }

    public function deleteVaccine(int $id)
    {
        $sql = "DELETE FROM vaccines WHERE id= :id";
        $params = [
            'id' => $id
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
    }
}
