<?php
require '../../class/Users.php';
require '../../class/Database.php';
class UsersRepository
{

    private
        $_db,
        $_createdAt;

    public function __construct()
    {
        $this->_db = new Database();
        $this->_db = $this->_db->getBDD();

        $this->_createdAt = new DateTimeImmutable();
    }

    // CRUD

    public function newUser(string $firstname, string $name, string $mail, string $password)
    {

        $sql = "INSERT INTO users (firstname, name, mail, password, createdAt, role) VALUES (:firstname, :name, :mail, :password, :createdAt, :role)";

        $params = [
            'firstname' => $firstname,
            'name' => $name,
            'mail' => $mail,
            'password' => $password,
            'createdAt' => $this->_createdAt->format('Y-m-d H:i:s'),
            'role' => FALSE
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
        $pdostmt->closeCursor();

        $userId = $this->_db->lastInsertId();
        $params['id'] = $userId;

        $user = new Users($params);
        return $user;
    }

    public function findOneUser(int $id)
    {
        $sql = "SELECT * FROM users WHERE id = :id";
        $params = [
            'id' => $id
        ];
        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
        return $pdostmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findAllUsers()
    {
        $sql = "SELECT * FROM users";
        $pdostmt = $this->_db->prepare($sql);
        return $pdostmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function updateUser(int $id, string $firstname, string $name, string $mail, string $password, bool $role)
    {
        $sql = "UPDATE users SET firstname= :firstname, name= :name, mail= :mail, password= :password, role= :role WHERE id= :id";
        $params = [
            'id' => $id,
            'firstname' => $firstname,
            'name' => $name,
            'mail' => $mail,
            'password' => $password,
            'role' => $role
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
    }

    public function deleteUser(int $id)
    {
        $sql = "DELETE FROM users WHERE id= :id";
        $params = [
            'id' => $id
        ];

        $pdostmt = $this->_db->prepare($sql);
        $pdostmt->execute($params);
    }
}
