<?php
class ToReceive
{

    private $_id,
        $_IdVaccines;

    public function __construct(array $infos)
    {
        $this->hydrate($infos);
    }

    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->_id;
    }

    // /**
    //  * Set the value of _id
    //  *
    //  * @return  self
    //  */
    // public function set_id($_id)
    // {
    //     $this->_id = $_id;

    //     return $this;
    // }

    /**
     * @return int
     */
    public function getIdVaccines(): int
    {
        return $this->_IdVaccines;
    }

    /**
     * @param int IdVaccines
     * @return  self
     */
    public function setIdVaccines(int $_IdVaccines): self
    {
        $this->_IdVaccines = $_IdVaccines;

        return $this;
    }
}
