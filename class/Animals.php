<?php
class Animals
{

    private $_id,
        $_Name,
        $_Type,
        $_Vaccine,
        $_InjectedAt,
        $_ID_users,
        $_ID_types;

    public function __construct(array $infos)
    {
        $this->hydrate($infos);
    }

    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @retunr int
     */
    public function getId(): int
    {
        return $this->_id;
    }

    /**
     * @param  int $_id
     * @return  self
     */
    public function setId(int $_id): self
    {
        $this->_id = $_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->_Name;
    }

    /**
     * @param string $_Name
     * @return  self
     */
    public function setName(string $_Name): self
    {
        $this->_Name = $_Name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->_Type;
    }

    /**
     * @param string $_Type
     * @return  self
     */
    public function setType(string $_Type): self
    {
        $this->_Type = $_Type;

        return $this;
    }

    /**
     * @return string
     */
    public function getVaccine(): string
    {
        return $this->_Vaccine;
    }

    /**
     * @param string $_Vaccine
     * @return  self
     */
    public function setVaccine(string $_Vaccine): self
    {
        $this->_Vaccine = $_Vaccine;

        return $this;
    }

    /**
     * @return DateTimeImmutable
     */
    public function getInjectedAt(): DateTimeImmutable
    {
        return $this->_InjectedAt;
    }

    /**
     * @param DateTimeImmutable $_InjectedAt
     * @return  self
     */
    public function setInjectedAt(DateTimeImmutable $_InjectedAt): self
    {
        $this->_InjectedAt = $_InjectedAt;

        return $this;
    }

    /**
     * Get the value of _ID_users
     */
    public function getIdUsers()
    {
        return $this->_ID_users;
    }

    // /**
    //  * Set the value of _ID_users
    //  *
    //  * @return  self
    //  */
    // public function setIdUsers($_ID_users)
    // {
    //     $this->_ID_users = $_ID_users;

    //     return $this;
    // }

    /**
     * Get the value of _ID_types
     */
    public function getIdTypes()
    {
        return $this->_ID_types;
    }

    /**
     * Set the value of _ID_types
     *
     * @return  self
     */
    // public function setIdTypes($_ID_types)
    // {
    //     $this->_ID_types = $_ID_types;

    //     return $this;
    // }
}
