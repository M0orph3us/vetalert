<?php

final class Database
{
    const DB_HOST = "localhost";
    const DB_BASE = "vetalert";
    const DB_USER = "VetAlert";
    const DB_MDP = "VetAlert";

    private $_BDD;

    public function __construct()
    {
        $this->connectBDD();
    }

    private function connectBDD()
    {
        try {
            $this->_BDD = new PDO("mysql:host=" . self::DB_HOST . ";dbname=" . self::DB_BASE, self::DB_USER, self::DB_MDP, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        } catch (PDOException $e) {
            die("Erreur de connexion : " . $e->getMessage());
        }
    }

    public function getBDD()
    {
        return $this->_BDD;
    }

    public function closeBDD()
    {
        $this->_BDD = null;
    }

    // public function initBDD()
    // {
    // }
}
