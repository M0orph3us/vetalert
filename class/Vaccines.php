<?php
class Vaccines
{

    private $_id,
        $_Name,
        $_Duration;

    public function __construct(array $infos)
    {
        $this->hydrate($infos);
    }

    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->_id;
    }

    /**
     * @param  int $_id
     * @return  self
     */
    public function setId(int $_id): self
    {
        $this->_id = $_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->_Name;
    }

    /**
     * @param string $_Name
     * @return  self
     */
    public function setName(string $_Name): self
    {
        $this->_Name = $_Name;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDuration(): DateTime
    {
        return $this->_Duration;
    }

    /**
     * @param DateTime $_Duration
     * @return  self
     */
    public function setDuration(Datetime $_Duration): self
    {
        $this->_Duration = $_Duration;

        return $this;
    }

    // Methods
}
