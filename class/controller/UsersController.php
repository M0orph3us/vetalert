<?php
require '../../class/repository/UsersRepository.php';

// functions for user register
function generateTokenCsrf()
{
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
    $tokenCsrfGenerate = bin2hex(random_bytes(32));
    $_SESSION['tokenCsrfGenerate'] = $tokenCsrfGenerate;
    return $tokenCsrfGenerate;
}


// Check CSRF when the form is send
function verifiedTokenCsrf()
{
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
    if (isset($_POST['tokenCsrfRegister'], $_SESSION['tokenCsrfGenerate']) && !empty($_POST['tokenCsrfRegister']) && !empty($_SESSION['tokenCsrfGenerate'])) {
        $tokenCsrfGenerate = $_SESSION['tokenCsrfGenerate'];
        $tokenCsrfInput = $_POST['tokenCsrfRegister'];

        if (hash_equals($tokenCsrfGenerate, $tokenCsrfInput)) {
            return TRUE;
        }
        return FALSE;
    }
}


function checkDoublePassword()
{

    if (
        (isset($_POST["password"], $_POST["checkPassword"]) && !empty($_POST["password"]) && !empty($_POST["checkPassword"]))
    ) {
        if ($_POST["password"] === $_POST["checkPassword"]) {
            return password_hash($_POST["password"], PASSWORD_DEFAULT);
        } else {
            throw new Exception("Your password is not the same. Please try again.");
        }
    }
}




function sanitizeInputRegisterForm(string $input): string
{
    $cleanedInput = trim($input);

    $cleanedInput = stripslashes($cleanedInput);

    $cleanedInput = htmlspecialchars($cleanedInput, ENT_QUOTES, 'UTF-8');

    return $cleanedInput;
}

function sanitizeRegisterForm()
{
    if (
        (isset($_POST["firstname"], $_POST["name"], $_POST["mail"]) && !empty($_POST["firstname"]) && !empty($_POST["name"]) && !empty($_POST["mail"]))
    ) {
        $sanitizedFirstname = sanitizeInputRegisterForm($_POST["firstname"]);
        $sanitizedName = sanitizeInputRegisterForm($_POST["name"]);
        $sanitizedMail = sanitizeInputRegisterForm($_POST["mail"]);
        return [$sanitizedFirstname, $sanitizedName, $sanitizedMail];
    }
}
function registerUser()
{
    if (verifiedTokenCsrf()) {
        $hashPassword = checkDoublePassword();
        $sanitizedInputs = sanitizeRegisterForm();

        if ($hashPassword && $sanitizedInputs) {
            list($sanitizedFirstname, $sanitizedName, $sanitizedMail) = $sanitizedInputs;
        }
        $user = new UsersRepository();
        $user->newUser($sanitizedFirstname, $sanitizedName, $sanitizedMail, $hashPassword);
        header("location:../../public/pages/index.php");
    }
}
registerUser();

// function for user login
