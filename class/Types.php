<?php
class Types
{

    private $_id,
        $_Specie;

    public function __construct(array $infos)
    {
        $this->hydrate($infos);
    }

    private function hydrate(array $array)
    {
        foreach ($array as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    /**
     * @retunr int
     */
    public function getId(): int
    {
        return $this->_id;
    }

    /**
     * @param  int $_id
     * @return  self
     */
    public function setId(int $_id): self
    {
        $this->_id = $_id;

        return $this;
    }

    /**
     * @return string
     */
    public function getSpecie(): string
    {
        return $this->_Specie;
    }

    /**
     * @param string $_Specie
     * @return  self
     */
    public function setSpecie(string $_Specie): self
    {
        $this->_Specie = $_Specie;

        return $this;
    }

    // Methods
}
