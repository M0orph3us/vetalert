<?php
$title = "Login";
require_once "../elements/header.php";

if (
    (isset($_POST["mail"], $_POST["password"]) && !empty($_POST["mail"]) && !empty($_POST["password"]))
) {
    $_SESSION['mail'] = $_POST['mail'];
    $_SESSION['password'] = $_POST['password'];
}
?>
<main class="loginContainer">
    <form action="#" method="POST" class="loginForm" id="loginForm">
        <label for="mail">Mail</label>
        <input type="mail" name="mail" id="logMail" placeholder="Enter your mail" required autocomplete="mail">

        <label for="password">Password</label>
        <input type="password" name="password" id="logPassword" placeholder="Enter your password" required>

        <input type="hidden" name="csrfTokenLog" value="csrfLog">
        <button type="button" class="btnLogin" id="btnLogin">Send</button>
    </form>
</main>
<?php require_once "../elements/footer.php" ?>