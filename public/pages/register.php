<?php
$title = "Register";
require_once "../elements/header.php";
require_once "../../class/controller/UsersController.php";
generateTokenCsrf();

?>
<main class="registerContainer">
    <form action="../../class/controller/UsersController.php" method="POST" class="registerForm" id="registerForm">
        <label for="firstname">Firstname</label>
        <input type="text" name="firstname" id="firstname" placeholder="Enter your firstname" required>

        <label for="name">Name</label>
        <input type="text" name="name" id="name" placeholder="Enter your name" required>

        <label for="mail">Mail</label>
        <input type="mail" name="mail" id="mail" placeholder="Enter your mail" required>

        <label for="password">Password</label>
        <input type="password" name="password" id="password" placeholder="Enter your password" required>

        <label for="checkPassword">Confirm your password</label>
        <input type="password" name="checkPassword" id="checkPassword" placeholder="Comfirm your password" required>

        <input type="hidden" name="tokenCsrfRegister" value="<?= $_SESSION['tokenCsrfGenerate']  ?>">
        <button type="submit" class="btnRegister" id="btnRegister">Send</button>
    </form>
</main>
<?php
require_once "../elements/footer.php";
?>