<?php
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../style/css/style.css">
    <script src="../script/script.js" defer></script>
    <title><?= $title ?></title>
</head>

<body>
    <header class="headerNavBar">
        <a href="../pages/index.php"><img class="logo" src="../img/logo.png" alt="Logo"></a>
        <ul class="links">
            <li><a href="../pages/index.php">HOME</a></li>
            <li><a href="../pages/animals.php">YOUR PET</a></li>
        </ul>
        <ul class="loginRegister">
            <li><a href="../pages/login.php">Login</a></li>
            <li><a href="../pages/register.php">Register</a></li>
        </ul>
        <a href="../pages/profil.php" style="display: none;">Profil</a>
    </header>